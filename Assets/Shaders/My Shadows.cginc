﻿#if !defined(MY_SHADOWS_INCLUDED)
#define MY_SHADOWS_INCLUDED

#include "UnityCG.cginc"


#define _RENDERING_CUTOUT


#if SHADOWS_SEMITRANSPARENT || defined(_RENDERING_CUTOUT)
	#if !defined(_SMOOTHNESS_ALBEDO)
		#define SHADOWS_NEED_UV 1
	#endif
#endif

float4 _Tint;
sampler2D _MainTex, _DepthTex;
float4 _MainTex_ST;
float _AlphaCutoff;

struct VertexData {
	float4 vertex : POSITION;
	float3 normal : NORMAL;
	float3 uv : TEXCOORD0;
};

struct InterpolatorsVertex {
	float4 position : SV_POSITION;
	#if SHADOWS_NEED_UV
		float3 uv : TEXCOORD0;
	#endif
	#if defined(SHADOWS_CUBE)
		float3 lightVec : TEXCOORD1;
	#endif

	float4 worldPos : TEXCOORD2;
};

struct Interpolators
{
	float4 positions : SV_POSITION;

	#if SHADOWS_NEED_UV
		float3 uv : TEXCOORD0;
	#endif
	#if defined(SHADOWS_CUBE)
		float3 lightVec : TEXCOORD1;
	#endif

	float4 worldPos : TEXCOORD2;
};

float GetAlpha (Interpolators i) {
	float alpha = _Tint.a;
	#if SHADOWS_NEED_UV
		alpha *= tex2D(_MainTex, i.uv.xy).a;
	#endif
	return alpha;
}

InterpolatorsVertex MyShadowVertexProgram (VertexData v) {
	InterpolatorsVertex i;
	#if defined(SHADOWS_CUBE)
		i.position = UnityObjectToClipPos(v.position);
		i.lightVec =
			mul(unity_ObjectToWorld, v.position).xyz - _LightPositionRange.xyz;
	#else
		i.position = UnityClipSpaceShadowCasterPos(v.vertex.xyz, v.normal);
		i.position = UnityApplyLinearShadowBias(i.position);
	#endif

	float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	i.worldPos.xyz = worldPos;
	COMPUTE_EYEDEPTH(i.worldPos.w);

	#if SHADOWS_NEED_UV
		i.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
	#endif
	i.uv.z = v.uv.z;
	return i;
}

float4 MyShadowFragmentProgram (Interpolators i,  out float outDepth : SV_Depth) : SV_TARGET
{
	float alpha = GetAlpha(i);
	#if defined(_RENDERING_CUTOUT)
		clip(alpha - _AlphaCutoff);
	#endif

	#if SHADOWS_SEMITRANSPARENT
		float dither =
			tex3D(_DitherMaskLOD, float3(i.vpos.xy * 0.25, alpha * 0.9375)).a;
		clip(dither - 0.01);
	#endif

	
	#if defined(SHADOWS_CUBE)
		float depth = length(i.lightVec) + unity_LightShadowBias.x;
		depth *= _LightPositionRange.w;
		return UnityEncodeCubeShadowDepth(depth);
	#else
		return 0;
	#endif

	fixed depthValue = tex2D(_DepthTex, i.uv).r;

	float depthWithOffset = i.worldPos.w + -depthValue.r * (i.uv.z * 0.5);

	outDepth = (1.0 - depthWithOffset * _ZBufferParams.w) / (depthWithOffset * _ZBufferParams.z);	
}

#if defined(SHADOWS_CUBE)

#endif

#endif