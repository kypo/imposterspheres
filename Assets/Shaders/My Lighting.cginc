﻿#if !defined(MY_LIGHTING_INCLUDED)
#define MY_LIGHTING_INCLUDED

#include "UnityPBSLighting.cginc"
#include "AutoLight.cginc"

float4 _Tint;
sampler2D _MainTex, _DepthTex, _NormalMap, _RampTex;
float4 _MainTex_ST;

float _BumpScale, _AlphaCutoff;

sampler2D _MetallicMap;
float _Metallic;
float _Smoothness;

struct VertexData 
{
	float4 vertex : POSITION;
	fixed4 color : COLOR;
	float3 normal : NORMAL;
	float4 tangent : TANGENT;
	float3 uv : TEXCOORD0; // Z is p.size
};

struct Interpolators 
{
	float4 pos : SV_POSITION;
	float4 uv : TEXCOORD0; // Z is p.size w is depth
	float3 normal : TEXCOORD1;
	fixed4 color : COLOR;

	#if defined(BINORMAL_PER_FRAGMENT)
		float4 tangent : TEXCOORD5;
	#else
		float3 tangent : TEXCOORD5;
		float3 binormal : TEXCOORD6;
	#endif

	float4 worldPos : TEXCOORD3;

	SHADOW_COORDS(4)

	#if defined(VERTEXLIGHT_ON)
		float3 vertexLightColor : TEXCOORD7;
	#endif

     UNITY_FOG_COORDS(2)
};

float3 GetAlbedo (Interpolators i) 
{	
	float3 albedo = tex2D(_MainTex, i.uv.xy).rgb * _Tint.rgb * i.color;
	return albedo;
}

float GetAlpha (Interpolators i) 
{
	float alpha = _Tint.a * i.color.a;
	#if !defined(_SMOOTHNESS_ALBEDO)
		alpha *= tex2D(_MainTex, i.uv.xy).a;
	#endif
	return alpha;
}

float3 GetTangentSpaceNormal (Interpolators i) 
{
	float3 normal = float3(0, 0, 1);
	#if defined(_NORMAL_MAP)
		normal = UnpackScaleNormal(tex2D(_NormalMap, i.uv.xy), _BumpScale);
	#endif
	return normal;
}

float GetMetallic (Interpolators i) 
{
	#if defined(_METALLIC_MAP)
		return tex2D(_MetallicMap, i.uv.xy).r;
	#else
		return _Metallic;
	#endif
}

float GetSmoothness (Interpolators i) 
{
	float smoothness = 1;
	#if defined(_SMOOTHNESS_ALBEDO)
		smoothness = tex2D(_MainTex, i.uv.xy).a;
	#elif defined(_SMOOTHNESS_METALLIC) && defined(_METALLIC_MAP)
		smoothness = tex2D(_MetallicMap, i.uv.xy).a;
	#endif
	return smoothness * _Smoothness;
}

void ComputeVertexLightColor (inout Interpolators i) 
{
	#if defined(VERTEXLIGHT_ON)
		i.vertexLightColor = Shade4PointLights(
			unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
			unity_LightColor[0].rgb, unity_LightColor[1].rgb,
			unity_LightColor[2].rgb, unity_LightColor[3].rgb,
			unity_4LightAtten0, i.worldPos, i.normal
		);
	#endif
}

float3 CreateBinormal (float3 normal, float3 tangent, float binormalSign) 
{
	return cross(normal, tangent.xyz) * (binormalSign * unity_WorldTransformParams.w);
}

UnityLight CreateLight (Interpolators i) 
{
	UnityLight light;

	#if defined(POINT) || defined(POINT_COOKIE) || defined(SPOT)
		light.dir = normalize(_WorldSpaceLightPos0.xyz - i.worldPos);
	#else
		light.dir = _WorldSpaceLightPos0.xyz;
	#endif

	UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos);
	
	light.color = _LightColor0.rgb * attenuation;
	light.ndotl = DotClamped(i.normal, light.dir);
	return light;
}

float3 BoxProjection (float3 direction, float3 position, float4 cubemapPosition, float3 boxMin, float3 boxMax) 
{
	#if UNITY_SPECCUBE_BOX_PROJECTION
		UNITY_BRANCH
		if (cubemapPosition.w > 0) 
		{
			float3 factors =
				((direction > 0 ? boxMax : boxMin) - position) / direction;
			float scalar = min(min(factors.x, factors.y), factors.z);
			direction = direction * scalar + (position - cubemapPosition);
		}
	#endif
	return direction;
}

UnityIndirect CreateIndirectLight (Interpolators i, float3 viewDir) 
{
	UnityIndirect indirectLight;
	indirectLight.diffuse = 0;
	indirectLight.specular = 0;

	#if defined(VERTEXLIGHT_ON)
		indirectLight.diffuse = i.vertexLightColor;
	#endif

	#if defined(FORWARD_BASE_PASS)
		indirectLight.diffuse += max(0, ShadeSH9(float4(i.normal, 1)));
		float3 reflectionDir = reflect(-viewDir, i.normal);
		Unity_GlossyEnvironmentData envData;
		envData.roughness = 1 - GetSmoothness(i);
		envData.reflUVW = BoxProjection(reflectionDir, i.worldPos, unity_SpecCube0_ProbePosition, unity_SpecCube0_BoxMin, unity_SpecCube0_BoxMax);
		float3 probe0 = Unity_GlossyEnvironment(UNITY_PASS_TEXCUBE(unity_SpecCube0), unity_SpecCube0_HDR, envData);
		envData.reflUVW = BoxProjection(reflectionDir, i.worldPos, unity_SpecCube1_ProbePosition, unity_SpecCube1_BoxMin, unity_SpecCube1_BoxMax);
		#if UNITY_SPECCUBE_BLENDING
			float interpolator = unity_SpecCube0_BoxMin.w;
			UNITY_BRANCH
			if (interpolator < 0.99999) 
			{
				float3 probe1 = Unity_GlossyEnvironment(UNITY_PASS_TEXCUBE_SAMPLER(unity_SpecCube1, unity_SpecCube0), unity_SpecCube0_HDR, envData);
				indirectLight.specular = lerp(probe1, probe0, interpolator);
			}
			else {
				indirectLight.specular = probe0;
			}
		#else
			indirectLight.specular = probe0;
		#endif
	#endif

	return indirectLight;
}

void InitializeFragmentNormal(inout Interpolators i) 
{
	float3 tangentSpaceNormal = GetTangentSpaceNormal(i);
	#if defined(BINORMAL_PER_FRAGMENT)
		float3 binormal = CreateBinormal(i.normal, i.tangent.xyz, i.tangent.w);
	#else
		float3 binormal = i.binormal;
	#endif
	
	i.normal = normalize(tangentSpaceNormal.x * i.tangent + tangentSpaceNormal.y * binormal + tangentSpaceNormal.z * i.normal);
}

Interpolators MyVertexProgram (VertexData v) 
{
	Interpolators i;

	//-------------------------------------------------------------------------------------------------

	i.pos = UnityObjectToClipPos(v.vertex);
	i.worldPos.xyz = mul(unity_ObjectToWorld, v.vertex).xyz;
	//COMPUTE_EYEDEPTH(i.worldPos.w);
	i.uv.w =  i.pos.z / i.pos.w; //Depth

	//-------------------------------------------------------------------------------------------------

	i.normal = UnityObjectToWorldNormal(v.normal);
	i.color = v.color;

	#if defined(BINORMAL_PER_FRAGMENT)
		i.tangent = float4(UnityObjectToWorldDir(v.tangent.xyz), v.tangent.w);
	#else
		i.tangent = UnityObjectToWorldDir(v.tangent.xyz);
		i.binormal = CreateBinormal(i.normal, i.tangent, v.tangent.w);
	#endif

	i.uv.z = v.uv.z; // Z is p.size
	i.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);

	TRANSFER_SHADOW(i);

	//ComputeVertexLightColor(i);

	 UNITY_TRANSFER_FOG(i,i.pos);

	return i;
}

inline float LinearEyeDepthPete( float z )
{
    return 1.0 / (_ZBufferParams.z * z + _ZBufferParams.w);
}


inline float LinearEyeDepthToOutDepthPete(float z)
{
    return (1 - _ZBufferParams.w * z) / (_ZBufferParams.z * z);
}

void MyFragmentProgram (Interpolators i, out fixed4 outColor : SV_Target,  out float outDepth : SV_Depth) 
{		
	float alpha = GetAlpha(i);
	clip(alpha - _AlphaCutoff);

	float3 worldPos = i.worldPos.xyz;

	InitializeFragmentNormal(i);

	float3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);

	float3 specularTint;
	float oneMinusReflectivity;
	float3 albedo = DiffuseAndSpecularFromMetallic(GetAlbedo(i), GetMetallic(i), specularTint, oneMinusReflectivity);

	float4 color = UNITY_BRDF_PBS(albedo, specularTint,	oneMinusReflectivity, GetSmoothness(i),	i.normal, viewDir, CreateLight(i), CreateIndirectLight(i, viewDir));
	half4 ramp = tex2D (_RampTex, float2(color.r, 1));
	ramp.a = 1;

	UNITY_APPLY_FOG(i.fogCoord, color);

	outColor = clamp(0,1,max(ramp,_Tint.a));

	//-------------------------------------------------------------------------------------------------
		
	float depthValue = tex2D(_DepthTex, i.uv).r ;
	float linearDepth = LinearEyeDepth(i.uv.w);

	float nonLinearDepthOffset = linearDepth + ((-depthValue) * (i.uv.z * 0.5));

	outDepth = LinearEyeDepthToOutDepthPete(nonLinearDepthOffset);

	//-------------------------------------------------------------------------------------------------
}

#endif